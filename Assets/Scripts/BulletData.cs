﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{
    public int Damage { get; set; }
    public float StunTime { get; set; }

    public void SetData(int damage, float stun)
    {
        Damage = damage;
        StunTime = stun;
    }
}
