﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ColumnsTilemap : MonoBehaviour
{
    private Tilemap tilemap;
    public Tile column;
    private int nColumns;

    // Start is called before the first frame update
    void Awake()
    {
        tilemap = GetComponent<Tilemap>();
    }

    public void GenerateColumns()
    {
        tilemap.ClearAllTiles();
        nColumns = Random.Range(5, 14);
        CustomUtilities.GenerateRandomCells(tilemap, column, nColumns);
    }
}
