﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DestructibleTilemap : MonoBehaviour
{
    private Tilemap tilemap;
    public Tile chest;
    private int nChests;

    // Start is called before the first frame update
    void Awake()
    {
        tilemap = GetComponent<Tilemap>();
    }

    public void GenerateChests()
    {
        tilemap.ClearAllTiles();
        nChests = Random.Range(1, 3);
        CustomUtilities.GenerateRandomCells(tilemap, chest, nChests);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Vector3Int cellPos = tilemap.WorldToCell(collision.gameObject.transform.position);
            if (tilemap.GetTile(cellPos) != null)
            {
                tilemap.SetTile(cellPos, null);
                Destroy(collision.gameObject);
                GameManager.Instance.InstantiateRandomItem(tilemap.GetCellCenterWorld(cellPos));
            }
        }
    }
}
