﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButton : MonoBehaviour
{
    private InventoryManager im;

    private Item item;

    private Button button;
    private Image image;
    private Text name;
    private Text price;
    public Sprite bought;

    // Start is called before the first frame update
    void Awake()
    {
        GameObject texts = transform.Find("TextFields").gameObject;
        GameObject buttonObj = transform.Find("Button").gameObject;

        im = GameManager.Instance.Player.GetComponent<InventoryManager>();

        button = buttonObj.GetComponent<Button>();
        image = buttonObj.GetComponent<Image>();
        name = texts.transform.GetChild(0).GetComponent<Text>();
        price = texts.transform.GetChild(1).GetComponent<Text>();
    }

    private void Start()
    {
        button.onClick.AddListener(ButtonLogic);
    }

    public void InitButton(Item newItem)
    {
        item = newItem;

        image.sprite = item.Image;
        name.text = item.Name;
        price.text = item.Price.ToString();
    }

    private void ButtonLogic()
    {
        if(GameManager.Instance.Score >= item.Price)
        {
            im.AddItem(item);
            GameManager.Instance.UpdateScore(-item.Price);
            image.sprite = bought;

            button.onClick.RemoveAllListeners();
        }
    }
}
