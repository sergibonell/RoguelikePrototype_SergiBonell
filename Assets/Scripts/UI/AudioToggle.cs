﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioToggle : MonoBehaviour
{
    private Toggle toggle;
    public AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        toggle = GetComponent<Toggle>();
        toggle.isOn = source.mute;
    }

    public void UpdateVolume()
    {
        source.mute = toggle.isOn;
        PlayerPrefs.SetInt(source.name + "mute", source.mute ? 1 : 0);
    }
}
