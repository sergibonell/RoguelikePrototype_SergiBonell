﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitScreenData : MonoBehaviour
{
    public Text HighScore;
    public Text LastScore;

    // Start is called before the first frame update
    void Start()
    {
        HighScore.text = PlayerPrefs.GetInt("best").ToString();
        LastScore.text = PlayerPrefs.GetInt("last").ToString();
        SoundManager.Instance.MusicAudioSource.Play();
    }
}
