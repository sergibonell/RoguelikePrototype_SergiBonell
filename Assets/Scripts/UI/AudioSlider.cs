﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSlider : MonoBehaviour
{
    private Slider slider;
    public AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.minValue = 0f;
        slider.maxValue = 1f;
        slider.value = source.volume;
    }

    public void UpdateVolume()
    {
        source.volume = slider.value;
        PlayerPrefs.SetFloat(source.name + "volume", source.volume);
    }
}
