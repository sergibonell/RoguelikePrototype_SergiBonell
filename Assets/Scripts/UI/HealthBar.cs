﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Slider slider;
    private RectTransform sliderRect;
    private CharacterData cData;

    // Start is called before the first frame update
    void Awake()
    {
        slider = GetComponent<Slider>();
        sliderRect = slider.GetComponent<RectTransform>();
        cData = GameManager.Instance.Player.GetComponent<CharacterData>();
    }

    public void UpdateSlider()
    {
        sliderRect.sizeDelta = new Vector2(60 * cData.MaxHp, 30);
        slider.maxValue = cData.MaxHp;
        slider.value = cData.CurrentHp;
    }
}
