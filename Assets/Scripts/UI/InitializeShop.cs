﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeShop : MonoBehaviour
{
    public List<Item> weaponsAvailable;
    public List<Item> consumablesAvailable;
    public List<Item> passivesAvailable;
    private ShopButton[] images;
    

    // Start is called before the first frame update
    void Start()
    {
        // M'asseguro que sempre hi hagi 2 items diferents de cada tipus
        int button = 0;
        images = GetComponentsInChildren<ShopButton>();

        for(; button < 2; button++)
            images[button].InitButton(GetRandomItem(weaponsAvailable));
        for(; button < 4; button++)
            images[button].InitButton(GetRandomItem(consumablesAvailable));
        for(; button < 6; button++)
            images[button].InitButton(GetRandomItem(passivesAvailable));
    }

    private Item GetRandomItem(List<Item> list)
    {
        int n = Random.Range(0, list.Count);
        Item result = list[n];
        list.RemoveAt(n);

        return result;
    }
}
