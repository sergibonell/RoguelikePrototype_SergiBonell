﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitEndData : MonoBehaviour
{
    public Text Score;
    public Text HighScore;
    public Text Enemies;
    public Text Rooms;
    public GameObject NewHighScore;
    public AudioClip DeathSound;

    // Start is called before the first frame update
    void Start()
    {
        Score.text += GameStats.Score.ToString();
        HighScore.text += PlayerPrefs.GetInt("best").ToString();
        Enemies.text += GameStats.EnemiesKilled.ToString();
        Rooms.text += GameStats.RoomsCompleted.ToString();
        NewHighScore.SetActive(GameStats.BeatHighScore);

        SoundManager.Instance.EffectsAudioSource.PlayOneShot(DeathSound);
    }
}
