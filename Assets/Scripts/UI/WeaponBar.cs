﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponBar : MonoBehaviour
{
    private WeaponManager wm;
    private List<Image> weaponIcon = new List<Image>();
    private Image selectedImage;

    // Start is called before the first frame update
    void Start()
    {
        wm = GameManager.Instance.Player.GetComponent<WeaponManager>();

        selectedImage = transform.GetChild(0).gameObject.GetComponent<Image>();
        for (int i = 1; i < transform.childCount; i++)
            weaponIcon.Add(transform.GetChild(i).gameObject.GetComponent<Image>());
    }

    public void UpdateWeaponUI()
    {
        for (int i = 0; i < weaponIcon.Count; i++)
        {
            weaponIcon[i].sprite = wm.GetWeaponImage(i);

            weaponIcon[i].preserveAspect = true;
            selectedImage.transform.position = weaponIcon[wm.CurrentWeapon].transform.position;
        }
    }
}
