﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float maxSpeed;
    public float dashCooldown;
    public float dashForce;

    private bool canDash = true;
    private Rigidbody2D rb;
    private CharacterData characterData;
    private Vector2 direction = Vector2.zero;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        characterData = GetComponent<CharacterData>();
    }

    // Update is called once per frame
    void Update()
    {
        direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (Input.GetKeyDown(KeyCode.Space) && canDash)
        {
            rb.AddForce(direction.normalized * dashForce, ForceMode2D.Impulse);
            StartCoroutine(dashReadyUp());
            StartCoroutine(characterData.InvulnerabilityDuration());
        }

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            characterData.anim.SetBool("Moving", true);
        else
            characterData.anim.SetBool("Moving", false);

        if (Input.GetAxis("Horizontal") < 0f)
            characterData.sr.flipX = true;
        else if(Input.GetAxis("Horizontal") > 0f)
            characterData.sr.flipX = false;

    }

    private void FixedUpdate()
    {
        rb.AddForce(direction * maxSpeed, ForceMode2D.Force);
        //transform.Translate(direction * Time.deltaTime * speed); #Can't use this because then collisions look weird
        //rb.velocity = direction * speed; #Can't use this because then I can't add extra forces (knockback)
    }

    IEnumerator dashReadyUp()
    {
        canDash = false;
        yield return new WaitForSeconds(dashCooldown);
        canDash = true;
    }
}
