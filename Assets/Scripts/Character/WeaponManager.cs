﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    private PlayerShooting shooting;

    public int MaxWeapons;
    public int CurrentWeapon;
    public Weapon InitialWeapon;

    public List<Weapon> weapons = new List<Weapon>();
    public Sprite Empty;

    // Start is called before the first frame update
    void Start()
    {
        shooting = GameManager.Instance.Player.GetComponentInChildren<PlayerShooting>();
        AddWeapon(InitialWeapon);
    }

    public bool AddWeapon(Weapon so)
    {
        bool grabbed = true;

        if (weapons.Count < MaxWeapons)
        {
            weapons.Add(so);
            CurrentWeapon = weapons.Count - 1;
            shooting.UpdateWeapon(weapons[CurrentWeapon]);
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            InstantiateCurrentWeapon(transform.position);
            weapons[CurrentWeapon] = so;
            shooting.UpdateWeapon(weapons[CurrentWeapon]);
        }
        else
            grabbed = false;

        return grabbed;
    }

    public void ChangeWeapon(int n)
    {
        CurrentWeapon = n;
        if (weapons.Count > n)
            shooting.UpdateWeapon(weapons[CurrentWeapon]);
        else
            shooting.RemoveWeapon();
    }

    public void DropCurrentWeapon()
    {
        if (CurrentWeapon < weapons.Count) 
        {
            Vector2 dropPosition = new Vector2(transform.position.x + 1, transform.position.y);
            InstantiateCurrentWeapon(dropPosition);
            weapons.RemoveAt(CurrentWeapon);
            shooting.RemoveWeapon();
        }
    }

    public void InstantiateCurrentWeapon(Vector2 position)
    {
        GameObject go = Instantiate(GameManager.Instance.Item, position, Quaternion.identity);
        go.GetComponent<ObjectSetup>().itemData = weapons[CurrentWeapon];
    }

    public Sprite GetWeaponImage(int n)
    {
        if (n < weapons.Count)
            return weapons[n].Image;
        else
            return Empty;//Sprite.Create(Texture2D.whiteTexture, Rect.MinMaxRect(1,1,2,2), Vector2.zero);
    }
}
