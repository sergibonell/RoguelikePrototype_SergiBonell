﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : Shooting
{
    private Rigidbody2D playerRB;
    private WeaponBar weaponUI;

    private void Start()
    {
        playerRB = GameManager.Instance.Player.GetComponent<Rigidbody2D>();
        weaponUI = GameManager.Instance.UIObject.GetComponentInChildren<WeaponBar>();
        target = GameObject.Find("Mouse").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Mouse0) && canShoot)
        {
            shootBullet();
            playerRB.AddForce(-direction * currentWeapon.Knockback, ForceMode2D.Impulse);
            StartCoroutine(bulletReadyUp());
        }
    }

    public void UpdateWeapon(Weapon so)
    {
        gameObject.SetActive(true);
        currentWeapon = so;
        StopAllCoroutines();
        canShoot = true;
        weaponUI.UpdateWeaponUI();

        if (spriteObject != null)
            Destroy(spriteObject);
        spriteObject = Instantiate(currentWeapon.WeaponObject, transform);
    }

    public void RemoveWeapon()
    {
        gameObject.SetActive(false);
        weaponUI.UpdateWeaponUI();
    }
}
