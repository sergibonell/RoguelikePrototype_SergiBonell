﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwap : MonoBehaviour
{
    private WeaponManager wm;

    // Start is called before the first frame update
    void Start()
    {
        wm = GameManager.Instance.Player.GetComponent<WeaponManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) || Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (wm.CurrentWeapon == 0)
                wm.ChangeWeapon(wm.MaxWeapons - 1);
            else
                wm.ChangeWeapon(wm.CurrentWeapon - 1);
        }else if (Input.GetKeyDown(KeyCode.C) || Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (wm.CurrentWeapon == wm.MaxWeapons - 1)
                wm.ChangeWeapon(0);
            else
                wm.ChangeWeapon(wm.CurrentWeapon + 1);
        }

        if(Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Mouse2))
        {
            wm.DropCurrentWeapon();
        }
    }
}
