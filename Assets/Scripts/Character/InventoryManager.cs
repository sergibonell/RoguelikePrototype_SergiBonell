﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public GameObject hoveringItem;

    private WeaponManager wm;
    private CharacterMovement cm;

    public List<Weapon> weapons = new List<Weapon>();

    private void Start()
    {
        wm = GetComponent<WeaponManager>();
        cm = GetComponent<CharacterMovement>();
    }

    private void Update()
    {
        if (hoveringItem != null)
        {
            Item i = hoveringItem.GetComponent<ObjectSetup>().itemData;
            AddItem(i);
        }
    }

    public void AddItem(Item so)
    {
        switch (so.ItemType)
        {
            case ItemTypes.Weapon:
                if (wm.AddWeapon((Weapon)so))
                    Destroy(hoveringItem);
                break;
            case ItemTypes.Consumable:
                applyConsumable((Consumable)so);
                Destroy(hoveringItem);
                break;
            case ItemTypes.Passive:
                applyPassive((Passive)so);
                Destroy(hoveringItem);
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Item"))
            hoveringItem = collision.gameObject;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Item"))
            hoveringItem = null;
    }

    private void applyConsumable(Consumable item)
    {
        switch (item.ConsumableType)
        {
            case (ConsumableTypes.HealthKit):
                GameManager.Instance.Player.GetComponent<CharacterData>().AddMaxHP(item.amount);
                break;
            case (ConsumableTypes.Potion):
                GameManager.Instance.Player.GetComponent<CharacterData>().AddHP(item.amount);
                break;
        }
    }

    private void applyPassive(Passive item)
    {
        foreach(StatModifier mod in item.modifiers)
        {
            switch (mod.stat)
            {
                case Stats.MoveSpeed:
                    cm.maxSpeed *= mod.value;
                    break;
                case Stats.DashCDR:
                    cm.dashCooldown *= mod.value;
                    break;
            }
        }
    }
}
