﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterData : MonoBehaviour
{
    private HealthBar hpBar;
    public Animator anim { get; private set; }
    public SpriteRenderer sr { get; private set; }

    private bool isInvulnerable = false;

    public int maxHp;
    public int MaxHp { get { return maxHp; } }

    private int currentHp = 0;
    public int CurrentHp { get { return currentHp; } }
    public AudioClip DamageSound;

    private void Start()
    {
        hpBar = GameManager.Instance.UIObject.GetComponentInChildren<HealthBar>();
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        AddHP(maxHp);
    }

    public void AddHP(int n)
    {
        if(currentHp+n <= maxHp)
        {
            currentHp += n;
            hpBar.UpdateSlider();
        }
    }

    public void AddMaxHP(int n)
    {
        currentHp += n;
        maxHp += n;
        hpBar.UpdateSlider();
    }

    public void TakeDamage(int n)
    {
        if (!isInvulnerable)
        {
            currentHp -= n;
            hpBar.UpdateSlider();
            if (currentHp <= 0)
                GameManager.Instance.GameOver();
            else
                SoundManager.Instance.EffectsAudioSource.PlayOneShot(DamageSound);
        }
    }

    public IEnumerator InvulnerabilityDuration()
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(0.3f);
        isInvulnerable = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            Destroy(collision.gameObject);
            TakeDamage(collision.gameObject.GetComponent<BulletData>().Damage);
        }
    }
}
