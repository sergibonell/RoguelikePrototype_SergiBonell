﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    protected Transform target;

    protected Weapon currentWeapon;
    protected GameObject spriteObject;
    protected bool canShoot = true;
    protected Vector2 direction;

    protected void shootBullet()
    {
        direction = (target.position - transform.position).normalized;
        float startAngle = Vector2.SignedAngle(Vector2.right, direction) + currentWeapon.DispersionAngle / 2;

        for (int i = 0; i < currentWeapon.NProjectiles; i++)
        {
            float angle = startAngle - currentWeapon.BulletSeparation * (currentWeapon.NProjectiles > 1 ? i : 1);
            Instantiate(currentWeapon.Bullet, spriteObject.transform.GetChild(0).position, Quaternion.AngleAxis(angle, Vector3.forward)).GetComponent<BulletData>().SetData(currentWeapon.Power, currentWeapon.StunTime);
            SoundManager.Instance.EffectsAudioSource.PlayOneShot(currentWeapon.ShootSound);
        }
    }

    protected IEnumerator bulletReadyUp()
    {
        canShoot = false;
        yield return new WaitForSeconds(currentWeapon.BulletCooldown);
        canShoot = true;
    }
}
