﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : Shooting
{
    public Weapon weaponSO;
    private EnemyData ed;

    private void Start()
    {
        currentWeapon = weaponSO;
        spriteObject = Instantiate(currentWeapon.WeaponObject, transform);
        target = GameObject.Find("Player").transform;
        ed = GetComponentInParent<EnemyData>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canShoot && !ed.Stunned)
        {
            shootBullet();
            StartCoroutine(bulletReadyUp());
        }
    }
}
