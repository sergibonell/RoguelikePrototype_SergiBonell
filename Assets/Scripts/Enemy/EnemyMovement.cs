﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    EnemyData ed;
    Rigidbody2D rb;
    public float Speed;
    public AudioClip explosion;
    public GameObject AnimationExplosion;

    private void Start()
    {
        ed = GetComponent<EnemyData>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!ed.Stunned)
        {
            Vector3 direction = (GameManager.Instance.Player.transform.position - transform.position).normalized;
            rb.velocity = direction * Speed;

            transform.Rotate(Vector3.forward, 360 * Time.deltaTime);
        }else
            rb.velocity = Vector2.zero;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Player.GetComponent<CharacterData>().TakeDamage(1); //avoid hardcode
            SoundManager.Instance.EffectsAudioSource.PlayOneShot(explosion);
            Instantiate(AnimationExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
            GameManager.Instance.roomManager.KilledEnemy();
        }
    }
}
