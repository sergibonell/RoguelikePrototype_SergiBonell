﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
    public bool Stunned = false;
    public int Hp;
    public int OnHitValue;
    public int KillValue;
    public AudioClip deathSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            BulletData bd = collision.gameObject.GetComponent<BulletData>();
            Hp -= bd.Damage;
            StartCoroutine(GetStunned(bd.StunTime));
            GameManager.Instance.UpdateScore(OnHitValue);
            if (Hp <= 0)
            {
                GameManager.Instance.UpdateScore(KillValue);
                Destroy(gameObject);
                GameManager.Instance.roomManager.KilledEnemy();
                SoundManager.Instance.EffectsAudioSource.PlayOneShot(deathSound);
            }
            Destroy(collision.gameObject);
        }
    }

    IEnumerator GetStunned(float time)
    {
        Stunned = true;
        yield return new WaitForSeconds(time);
        Stunned = false;
    }
}
