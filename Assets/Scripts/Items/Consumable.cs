﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Consumables", menuName = "ScriptableObjects/Items/Consumables", order = 3)]
public class Consumable : Item
{
    public ConsumableTypes ConsumableType;
    public int amount;

    private void Awake()
    {
        ItemType = ItemTypes.Consumable;
    }
}

public enum ConsumableTypes
{
    Potion,
    HealthKit
}
