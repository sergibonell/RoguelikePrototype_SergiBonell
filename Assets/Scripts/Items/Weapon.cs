﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Items/Weapon", order = 1)]
public class Weapon : Item
{
    public GameObject WeaponObject;
    public GameObject Bullet;
    public int StartingAmmo;
    public int Power;
    public int NProjectiles;
    public float BulletsPerSecond;
    public float DispersionAngle;
    public float Knockback;
    public float StunTime;
    public AudioClip ShootSound;
    
    public float BulletCooldown { get { return 1 / BulletsPerSecond; } }
    public float BulletSeparation { get { return DispersionAngle / (NProjectiles > 1 ? NProjectiles - 1 : 2); } }

    private void Awake()
    {
        ItemType = ItemTypes.Weapon;
    }
}
