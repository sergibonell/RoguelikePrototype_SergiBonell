﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Passive", menuName = "ScriptableObjects/Items/Passive", order = 2)]
public class Passive : Item
{
    public List<StatModifier> modifiers;

    private void Awake()
    {
        ItemType = ItemTypes.Passive;
    }
}

[System.Serializable]
public struct StatModifier
{
    public float value;
    public Stats stat;
}

public enum Stats
{
    MoveSpeed,
    DashCDR
}
