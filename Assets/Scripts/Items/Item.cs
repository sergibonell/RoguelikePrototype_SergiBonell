﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : ScriptableObject
{
    public string Name;
    public int Price;
    public Sprite Image;
    public ItemTypes ItemType;
}

public enum ItemTypes{
    Weapon,
    Passive,
    Consumable
}
