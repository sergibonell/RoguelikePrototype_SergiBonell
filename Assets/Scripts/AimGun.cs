﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimGun : MonoBehaviour
{
    private Transform target;
    protected Vector2 direction;
    public float angle;
    public bool enemy;

    // Utilitzo el mateix script pel jugador i els enemics, depenent del bool seguiré al mouse o al jugador
    private void Start()
    {
        if(enemy)
            target = GameObject.Find("Player").transform;
        else
            target = GameObject.Find("Mouse").transform;
    }

    // Calculo l'angle entre el target i l'objecte i roto
    void Update()
    {
        direction = (target.position - transform.position).normalized;
        angle = Vector2.SignedAngle(Vector2.right, direction);

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
