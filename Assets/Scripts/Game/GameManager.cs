﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public RoomManager roomManager;
    private Vector2 _cameraSize;

    public GameObject Player;
    public GameObject[] Enemies;
    public GameObject UIObject;
    public GameObject Shop;
    public GameObject Item;
    public ItemPool DroppableItems;

    public Vector2 CameraSize { get { return _cameraSize; } }
    public Vector2 MousePosition { get; private set; }
    public int Score { get; private set; }
    public int SpentScore { get; private set; }
    public BoundsInt CellSize { get; private set; }

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;

        CellSize = GameObject.Find("Tilemap Base").GetComponent<Tilemap>().cellBounds;
    }

    private void Start()
    {
        UpdateCameraSize();
        SoundManager.Instance.MusicAudioSource.Play();
    }

    private void Update()
    {
        MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.B))
        {
            Shop.SetActive(!Shop.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            GameOver();
        }
    }

    public void UpdateCameraSize()
    {
        _cameraSize = new Vector2(Camera.main.orthographicSize * Screen.width / Screen.height, Camera.main.orthographicSize);
    }

    public void UpdateScore(int n)
    {
        if (n < 0)
            SpentScore -= n;
        Score += n;
        UIObject.GetComponentInChildren<ScoreUpdate>().UpdateScore();
    }

    public void GameOver()
    {
        int totalScore = Score + SpentScore;

        PlayerPrefs.SetInt("last", totalScore);
        if (PlayerPrefs.GetInt("best") < totalScore)
        {
            PlayerPrefs.SetInt("best", totalScore);
            GameStats.BeatHighScore = true;
        }
        else
            GameStats.BeatHighScore = false;

        GameStats.Score = totalScore;
        GameStats.RoomsCompleted = roomManager.roomsCleared;
        GameStats.EnemiesKilled = roomManager.enemiesKilled;

        SoundManager.Instance.MusicAudioSource.Stop();
        SceneManager.LoadScene("EndScreen");
    }

    public void InstantiateRandomItem(Vector2 position)
    {
        GameObject go = Instantiate(Item, position, Quaternion.identity);
        go.GetComponent<ObjectSetup>().itemData = DroppableItems.GetRandomItem();
    }
}
