﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameStats
{
    public static int Score { get; set; }
    public static int EnemiesKilled { get; set; }
    public static int RoomsCompleted { get; set; }
    public static bool BeatHighScore { get; set; }

}
