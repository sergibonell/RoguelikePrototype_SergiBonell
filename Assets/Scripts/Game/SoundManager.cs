﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// He mirat el AudioMixer però no em convenç per silenciar sons des de codi
public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance = null;
    public AudioSource EffectsAudioSource;
    public AudioSource MusicAudioSource;

    public static SoundManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            //First run, set the instance
            _instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else if (_instance != this)
        {
            //Instance is not the same as the one we have, destroy old one, and reset to newest one
            Destroy(_instance.gameObject);
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        EffectsAudioSource.volume = PlayerPrefs.GetFloat(EffectsAudioSource.name + "volume");
        MusicAudioSource.volume = PlayerPrefs.GetFloat(MusicAudioSource.name + "volume");
        EffectsAudioSource.mute = PlayerPrefs.GetInt(EffectsAudioSource.name + "mute") == 1;
        MusicAudioSource.mute = PlayerPrefs.GetInt(MusicAudioSource.name + "mute") == 1;
    }
}
