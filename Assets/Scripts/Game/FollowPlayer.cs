﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    Transform player;
    Vector2 maxBounds;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.Instance.Player.transform;
        maxBounds = new Vector2(GameManager.Instance.CameraSize.x - 4f, GameManager.Instance.CameraSize.y - 2f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(player.position.x, -maxBounds.x, maxBounds.x), Mathf.Clamp(player.position.y, -maxBounds.y, maxBounds.y), transform.position.z);
    }
}
