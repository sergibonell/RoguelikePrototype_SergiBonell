﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteOnCollision : MonoBehaviour
{
    public List<string> tags;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (tags.Contains(collision.gameObject.tag))
            Destroy(gameObject);
    }
}
