﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomManager : MonoBehaviour
{
    public int roomsCleared = 0;
    public int enemiesKilled = 0;
    private int nEnemies;
    private GameObject doors;
    private ColumnsTilemap columns;
    private DestructibleTilemap destructible;
    private RoomUpdate roomUI;

    // Start is called before the first frame update
    void Start()
    {
        doors = transform.Find("Tilemap Doors").gameObject;
        columns = transform.GetComponentInChildren<ColumnsTilemap>();
        destructible = transform.GetComponentInChildren<DestructibleTilemap>();
        roomUI = GameManager.Instance.UIObject.GetComponentInChildren<RoomUpdate>();
        
        generateRoom();
    }

    private void generateEnemies()
    {
        nEnemies = Random.Range(1 + roomsCleared, 3 + roomsCleared);
        for (int i = 0; i<nEnemies; i++)
        {
            int enemy = Random.Range(0, GameManager.Instance.Enemies.Length);
            Vector3Int position = CustomUtilities.GetRandomCell(new Vector2Int(9, 5));

            Instantiate(GameManager.Instance.Enemies[enemy], position, Quaternion.identity);
        }
    }

    private void generateRoom()
    {
        doors.SetActive(true);

        generateEnemies();
        columns.GenerateColumns();
        destructible.GenerateChests();
        roomUI.UpdateRoom(roomsCleared);
    }

    public void KilledEnemy()
    {
        enemiesKilled++;
        if(--nEnemies == 0)
        {
            doors.SetActive(false);
            roomsCleared++;
            GameManager.Instance.UpdateScore(100);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Player.transform.position = new Vector3(0, 0, GameManager.Instance.Player.transform.position.z);
            RemoveItemsFromScene();
            generateRoom();
        }
    }

    private void RemoveItemsFromScene()
    {
        GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
        foreach(GameObject item in items)
        {
            Destroy(item);
        }
    }
}
