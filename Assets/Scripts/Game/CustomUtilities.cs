﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class CustomUtilities
{
    public static Vector2 GetMouseDirection(Vector3 origin)
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition) - origin;
    }

    public static int VectorToCardinal(Vector2 v)
    {
        //0=EAST, 1=NORTH, 2=WEST, 3=SOUTH
        return Mathf.RoundToInt((Mathf.Atan2(v.y, v.x) * 2 / Mathf.PI) + 4) % 4;
    }

    public static Vector3Int GetRandomCell(Vector2Int layout)
    {
        Vector3Int cell;

        cell = new Vector3Int(Random.Range(-layout.x, layout.x), Random.Range(-layout.y, layout.y), 0);

        return cell;
    }

    public static void GenerateRandomCells(Tilemap tilemap, Tile tile, int n)
    {
        for (int i = 0; i < n; i++)
        {
            Vector3Int pos;
            pos = CustomUtilities.GetRandomCell(new Vector2Int(8, 4));
            tilemap.SetTile(pos, tile);
        }
    }
}
