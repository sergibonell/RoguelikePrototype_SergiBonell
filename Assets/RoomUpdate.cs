﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomUpdate : MonoBehaviour
{
    private Text room;

    void Start()
    {
        room = GetComponent<Text>();
    }

    public void UpdateRoom(int n)
    {
        room.text = "Room " + n;
    }
}
