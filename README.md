
# RoguelikePrototype_SergiBonell

## Controls

- **WASD**: Move the main character
- **Space**: Dash (it has a cooldown)
- **Left Click**: Shoot with the currently equipped gun
- **Z/C** or **ScroollDown/ScrollUp**: Cycle through currently owned weapons (3 max)
- **F**: Swap current weapon for one on the ground (only if inventory full)
- **X**: Drop current weapon
- **B**: Open/close shop
- **R**: Go to end screen directly (in case you get stuck)
